/**
 * Created by Spirits on 16. 7. 6..
 */

package com.zikto.arki;

public class QuickstartPrefrence {
    public static final String REGISTRATION_READY = "registrationReady";
    public static final String REGISTRATION_GENERATING = "registrationGenerating";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}
